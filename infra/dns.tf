resource "yandex_dns_zone" "zone" {
  name = "php-project-zone"

  zone   = "php.baner.space."
  public = true
}

resource "yandex_dns_recordset" "gitlab" {
  zone_id = yandex_dns_zone.zone.id
  name    = "gitlab.php.baner.space."
  type    = "A"
  ttl     = 200
  data    = [module.yc-instance["gitlab"].external_instance_ip]
}

resource "yandex_dns_recordset" "stage" {
  zone_id = yandex_dns_zone.zone.id
  name    = "travellist.stage.php.baner.space."
  type    = "A"
  ttl     = 200
  data    = [module.yc-instance["app01"].external_instance_ip]
}

resource "yandex_dns_recordset" "prod" {
  zone_id = yandex_dns_zone.zone.id
  name    = "travellist.prod.php.baner.space."
  type    = "A"
  ttl     = 200
  data    = [module.yc-instance["prod01"].external_instance_ip]
}
