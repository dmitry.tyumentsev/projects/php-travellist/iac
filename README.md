# Инфраструктурный репозиторий 

## Запуск инфраструктуры в Yandex Cloud

### Склонировать проект
`git clone https://gitlab.com/dmitry.tyumentsev/projects/php-travellist/iac.git`

`cd iac`

### Установить ansible, yandex cloud sdk
`pip install -r requirements.txt`

### Установить ansible роли
`ansible-galaxy install -r requirements.yml`

### Указать секретные переменные, с помощью [yc](https://cloud.yandex.ru/docs/cli/operations/install-cli)
```shell
export TF_VAR_yc_iam_token=$(yc iam create-token)
export TF_VAR_yc_cloud_id=$(yc config get cloud-id)
export TF_VAR_yc_folder_id=$(yc config get folder-id)
```

### Инициализация terraform с использованием Yandex Object Storage в качестве бэкэнда

`cd infra`

`terraform init -backend-config="access_key=$ACCESS_KEY" -backend-config="secret_key=$SECRET_KEY"`

### Поднять виртуальные машины

`terraform apply`

### Запуск плейбука
`cd ..`

`chmod +x inventories/hosts.py`

Указать пароль для Ansible Vault в файле `pass`

Зашифровать переменные `proxy_url`, `gitlab_root_password`

`ansible-vault encrypt_string --vault-id pass 'password' --name 'gitlab_root_password'`

`ansible-vault encrypt_string --vault-id pass 'http://...' --name 'proxy_url'`

Указать их в `inventories/group_vars/gitlab.yml`

`ansible-playbook infra_playbook.yml -i inventories/hosts.py --vault-id pass`

Конфиг подключения к openVPN будет в `./ovpn/`.

### Удаление виртуальных машин

`cd infra`

`terraform destroy`

## Деплой Laravel приложения

Осуществляется автоматическии при изменении ветки `main` [репозитория приложения](https://gitlab.com/dmitry.tyumentsev/projects/php-travellist/app).
